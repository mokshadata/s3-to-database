import logging
import os
import sys
import argparse

from urllib.parse import urlparse
from dotenv import load_dotenv
from smart_open import smart_open

parser = argparse.ArgumentParser()
parser.add_argument("--debug", help="show debug logs", action="store_true")
args = parser.parse_args()
log_level = logging.INFO
if args.debug:
    log_level = logging.DEBUG
logging.basicConfig(stream=sys.stdout, level=log_level, format='%(message)s')
logger = logging.getLogger(__name__)


def uppercase(f, *args, **kwargs):
    def func(*args, **kwargs):
        s = f(*args, **kwargs)
        return str(s).upper()

    return func


@uppercase
def evaluate_type(types):
    types = set(types)

    if 'NULL' in types:
        types.remove('NULL')

    if len(types) == 1:
        # if there is only one type
        return list(types)[0]
    elif len(types) == 0:
        # if null is the only type  in the set
        return "varchar(max)"

    type_hierarchy = [
        'varchar(max)',
        'timestamptz'
        'datetime',
        'date',
        'numeric(25,7)',
        'bigint',
        'integer',
        'boolean',
    ]

    for _type in type_hierarchy:
        if _type in types:
            return _type


def split_table_name(table_name_with_schema):
    split_name = table_name_with_schema.split('.')
    return tuple(split_name)


def get_columns_names(s3_path):
    for line in smart_open(s3_path, 'rb'):
        column_names = line.decode()
        column_names = column_names.split(',')
        for i, name in enumerate(column_names):
            try:
                column_names[i] = name.strip()
                column_names[i] = name.decode()
            except (UnicodeDecodeError, AttributeError):
                pass
        return column_names


def s3_to_database():
    load_dotenv()
    server = os.getenv('DB_HOST')
    database = os.getenv('DB_DATABASE')
    username = os.getenv('DB_USERNAME', '')
    if not username:
        username = os.getenv('DB_CREDENTIAL_USERNAME', '')
    password = os.getenv('DB_PASSWORD', '')
    if not password:
        password = os.getenv('DB_CREDENTIAL_PASSWORD', '')
    conn = get_redshift_conn(server, database, username, password)

    db = Database(conn)

    # paths of files to write
    s3_file_paths = os.getenv('S3_PATHS').split('\n')
    # names of tables to be written to (with schema) example: temp.test_table
    table_names = os.getenv('TABLE_NAMES').split('\n')
    # allows table_name to be referenced in except block
    errors = []
    for s3_path, table in zip(s3_file_paths, table_names):
        table_schema, table_name = split_table_name(table)
        try:
            s3_path_parts = urlparse(s3_path, allow_fragments=False)
            bucket = s3_path_parts.netloc
            key = s3_path_parts.path.lstrip('/')
            logger.info(f'\nbucket: {bucket}')
            logger.info(f'path: {key}')
            split_name = split_table_name(table)
            if len(split_name) != 2:
                raise Exception('Error: table name format is incorrect. Use this format: schema.table_name')

            full_path = f'{bucket}/{key}'
            # get first row of csv for column names
            column_names = get_columns_names(s3_path)
            csv_stream = csv_to_arrow(bucket, full_path, column_names)
            logger.info('Csv stream created')
            batch_count = 1
            specs = {}
            # initialize each column with empty set that will contain type guesses
            for col in column_names:
                specs[col] = set()

            for batch in csv_stream:
                logger.info(f'Fetched batch {batch_count} from: {s3_path}')
                logger.info('Getting metadata stats')
                metadata = get_metadata_stats(batch)
                logger.debug(f'Metadata: {metadata}')
                for column, metadata in metadata['columns'].items():
                    for type_guess in metadata['type']:
                        specs[column].add(type_guess)
                logger.info(f'Batch {batch_count} processing complete')
                batch_count += 1
            logger.info('End of file stream reached')
            logger.info('Evaluating types')
            resolved_specs = {column: evaluate_type(type_guesses) for column, type_guesses in specs.items()}

            table_exists = does_table_exist(conn, table_name, table_schema)
            if table_exists:
                logger.info(f'Found existing table: {table_schema}.{table_name}')
                table_name = f'new_{table_name}'
            logger.info(f'Creating table {table_schema}.{table_name}')
            db.create_table(table_name, table_schema, resolved_specs)
            db.copy_table(table_name, table_schema, s3_path)
            result = 'success'

            # check that both orig_table and new_table exist before doing swap
            new_table_exists = does_table_exist(conn, table_name, table_schema)
            if table_exists and new_table_exists:
                result = truncate_and_load_new(conn, table_name, table_schema)

            if result == 'error':
                raise Exception('Error: schema mismatch or empty new_ table')
        except Exception:
            logger.exception(f'Error loading from {s3_path} into {table}')
            if 'new_' in table_name:
                drop_table(conn, table_name, table_schema)
            errors.append(table)

    conn.commit()
    conn.close()

    if len(errors) > 0:
        raise Exception(f'Error: the following tables failed to load: {errors}')


if __name__ == "__main__":
    from inspector import csv_to_arrow, get_metadata_stats
    from db import get_redshift_conn, Database, does_table_exist, drop_table, truncate_and_load_new
    s3_to_database()
else:
    from .inspector import csv_to_arrow, get_metadata_stats
    from .db import get_redshift_conn, Database, does_table_exist, drop_table, truncate_and_load_new
