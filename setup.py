from distutils.core import setup

install_requires = [
    'boto3>=1.17.40',
    'botocore>=1.20.40',
    'colorama>=0.4.4',
    'commonmark>=0.9.1',
    'jmespath>=0.10.0',
    'numpy>=1.20.2',
    'pendulum>=2.1.2',
    'psycopg2-binary>=2.8.6',
    'pyarrow>=3.0.0',
    'Pygments>=2.8.1',
    'python-dateutil>=2.8.2',
    'python-dotenv>=0.19.0',
    'pytzdata>=2020.1',
    'rich>=10.1.0',
    's3transfer>=0.5.0',
    'six>=1.16.0',
    'smart-open>=5.2.1',
    'typing-extensions>=3.7.4.3',
    'urllib3>=1.26.6',
]

setup(
    name='s3_to_database',
    version='0.9',
    packages=['s3_to_database'],
    long_description='Writes files from S3 into tables in a database',
    install_requires=install_requires,
)
