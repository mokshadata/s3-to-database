import psycopg2
import logging

logger = logging.getLogger(__name__)


class Database:
    def __init__(self, conn):
        self.conn = conn

    def create_table(self, table_name, table_schema, specs):
        conn = self.conn
        sql = self.create_table_sql(table_name, table_schema, specs)

        with conn.cursor() as cursor:
            try:
                logger.info(f'Running SQL:\n{sql}')
                cursor.execute(sql)
                logger.info(f'Table created: {table_schema}.{table_name}')
                conn.commit()
                return sql
            except Exception:
                logger.exception("Table creation failed: Could not create {table}".format(
                    table=table_name))
                raise

    def copy_table(self, table_name, table_schema, s3_path):
        conn = self.conn

        sql = f"""
            COPY {table_schema}.{table_name}
            FROM '{s3_path}'
            iam_role 'arn:aws:iam::006484606258:role/RedshiftCopyFromS3'
            region 'us-east-1'
            DELIMITER ','
            CSV
            IGNOREHEADER 1
            dateformat 'auto'
            timeformat 'auto';
        """

        with conn.cursor() as cursor:
            try:
                cursor.execute(sql)
            except Exception:
                logger.exception("Copying Data Failed for Table {table}".format(
                    table=table_name))
                raise
            finally:
                conn.commit()

    def create_table_sql(self, table_name, table_schema, table_specs):
        columns_str = ",".join(["\n\t\"{0}\" {1}".format(x[0], x[1]) for x in table_specs.items()])

        sql = """CREATE TABLE {schema}.{table_name} ({columns_str}\n)"""
        sql = sql.format(schema=table_schema, table_name=table_name, columns_str=columns_str)

        return sql


def get_redshift_conn(server, database, username, password, port=5432):
    return psycopg2.connect(
        dbname=database, user=username, port=port,
        host=server, password=password, sslmode='require'
    )


def execute_sql(conn, sql):
    with conn.cursor() as cursor:
        cursor.execute(sql)
        try:
            return cursor.fetchall()
        except Exception as e:
            return None


def does_table_exist(conn, table_name, schema='public'):
    sql = """
        SELECT 1 
        FROM information_schema.tables
        WHERE table_schema='{schema}' AND table_name='{table_name}';
    """.format(table_name=table_name, schema=schema)
    result = execute_sql(conn, sql)
    return bool(result)


def get_table_spec(conn, table_name, schema):
    sql = """
        SELECT
            column_name,
            data_type
        FROM information_schema.columns
        WHERE TABLE_NAME='{table_name}' AND table_schema='{schema}'
        ORDER BY ordinal_position ASC
    """.format(table_name=table_name, schema=schema)
    return execute_sql(conn, sql)


def truncate_table(conn, table_name, schema):
    sql = """
        TRUNCATE TABLE {schema}.{table_name};
    """.format(schema=schema, table_name=table_name)
    return execute_sql(conn, sql)


def drop_table(conn, table_name, schema):
    sql = """
        DROP TABLE IF EXISTS {schema}.{table_name};
    """.format(schema=schema, table_name=table_name)
    return execute_sql(conn, sql)


def load_table(conn, from_table, to_table, schema):
    sql = """
        INSERT INTO {schema}.{to_table}
        SELECT * FROM {schema}.{from_table};
    """.format(schema=schema, from_table=from_table, to_table=to_table)
    return execute_sql(conn, sql)


def table_has_rows(conn, new_table_schema, new_table):
    sql = f"SELECT 1 FROM {new_table_schema}.{new_table};"
    result = execute_sql(conn, sql)
    return bool(result)


def truncate_and_load_new(conn, new_table_name, table_schema):
    if not table_has_rows(conn, table_schema, new_table_name):
        logger.error(f'Error: new table {table_schema}.{new_table_name} is empty')
        logger.info(f'Deleting new table: {table_schema}.{new_table_name}')
        drop_table(conn, new_table_name, table_schema)
        return 'error'

    # remove the 'new_' prefix to get orig_table_name
    orig_table_name = new_table_name[4:]
    # print(f'orig table_name: {orig_table_name}')
    # print(f'new table_name: {new_table_name}')
    # print(f'table_schema: {table_schema}')
    orig_table_name = orig_table_name.strip()
    new_table_name = new_table_name.strip()

    new_table_spec = get_table_spec(conn, new_table_name, table_schema)
    orig_table_spec = get_table_spec(conn, orig_table_name, table_schema)

    # double check orig_table exists before truncating it for table swap
    if not does_table_exist(conn, orig_table_name, table_schema):
        logger.error(f'Error: table does not exist: {table_schema}.{orig_table_name}')
        logger.info(f'Deleting new table: {table_schema}.{new_table_name}')
        drop_table(conn, new_table_name, table_schema)
        return 'error'

    if new_table_spec == orig_table_spec:
        logger.info(f'Truncating existing table: {table_schema}.{orig_table_name}')
        truncate_table(conn, orig_table_name, table_schema)
        logger.info(f'Loading new table data from {table_schema}.{new_table_name} into existing table {table_schema}.{orig_table_name}')
        load_table(conn, from_table=new_table_name, to_table=orig_table_name, schema=table_schema)
        result = 'success'
    else:
        logger.error(f"""Table schemas do not match: {orig_table_name} & {new_table_name}
                      new table spec:
                      {new_table_spec}
                      existing table spec:
                      {orig_table_spec}""")
        result = 'error'

    logger.info(f'Deleting new table: {table_schema}.{new_table_name}')
    drop_table(conn, new_table_name, table_schema)
    return result
