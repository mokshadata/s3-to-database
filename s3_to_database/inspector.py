from collections import OrderedDict
import logging

import pendulum
import pyarrow.compute as pc
import pyarrow as pa
from pyarrow import fs, csv

logger = logging.getLogger(__name__)


def open_file_stream(bucket, file_path):
    s3, path = fs.FileSystem.from_uri(f's3://{bucket}')
    return s3.open_input_stream(file_path)


def csv_to_arrow(bucket, file_path, column_names):
    schema_all_strings = {}
    for name in column_names:
        schema_all_strings[name] = pa.string()
    csv_options = csv.ConvertOptions(
        null_values=['', ' '],
        false_values=['', ' '],
        strings_can_be_null=True,
        timestamp_parsers=[
            '%Y-%m-%dT%H:%M:%S.%f%z',
        ],
        column_types=schema_all_strings
    )
    # cap memory usage to block size (bytes)
    read_options = csv.ReadOptions(
        block_size=1073741824
    )
    f = open_file_stream(bucket, file_path)
    return csv.open_csv(f, convert_options=csv_options, read_options=read_options)


def is_boolean(val):
    valid = set(['False', 'True', 'true', 'false', 'TRUE', 'FALSE', 't', 'f',
                'yes', 'no', 'y', 'n'])
    if val in valid:
        return True
    return False


def is_decimal(val):
    if val == '0.0':
        return True
    try:
        return isinstance(float(val), float)
    except:
        return False


def is_int(val):
    if '.' in val:
        return False
    if val == '0':
        return True
    try:
        val_to_int = int(val)
        if abs(val_to_int) > 2147483646:
            return False
        return bool(val_to_int)
    except:
        return False


def is_bigint(val):
    if '.' in val:
        return False
    try:
        val_to_int = int(val)
        if abs(val_to_int) > 2147483646:
            return bool(val_to_int)
    except:
        return False


def is_date(val):
    if ':' in val:
        return False
    try:
        dt = pendulum.parse(val)
        if dt.hour == 0 and dt.minute == 0 and dt.second == 0:
            return True
    except:
        return False


def is_datetime(val):
    if ':' not in val:
        return False
    try:
        dt = pendulum.parse(val)
        if dt.tzinfo is not None:
            return False
        return bool(dt)
    except:
        return False


def is_datetime_with_tz(val):
    if ':' not in val:
        return False
    try:
        dt = pendulum.parse(val)
        if dt.tzinfo is None:
            return False
        return bool(dt)
    except:
        return False


def guess_type(val):
    types = OrderedDict([
        ('boolean', is_boolean),
        ('integer', is_int),
        ('bigint', is_bigint),
        ('numeric(25,7)', is_decimal),
        ('date', is_date),
        ('datetime', is_datetime),
        ('timestamptz', is_datetime_with_tz),
    ])
    _val = val.strip()
    if _val == 'None':
        return 'NULL'

    guess = 'varchar(max)'
    for typ, test in types.items():
        result = test(_val)
        if result:
            guess = typ
            break
    return guess


def guess_col_type(data):
    guesses = set()
    total_skipped = 0
    for val in data:
        if 'varchar(max)' in guesses:
            break
        try:
            guess = guess_type(str(val))
            guesses.add(guess)
        except:
            total_skipped += 1
    return (guesses, total_skipped)


def get_metadata_stats(arrow_table):
    metadata = {
        'num_rows': arrow_table.num_rows,
        'columns': {}
    }
    try:
        num_columns = arrow_table.num_columns
        for col_idx in range(0, num_columns):
            col = arrow_table.field(col_idx).name
            logger.debug(f'Getting metadata for column: {col}')
            distinct = pc.unique(arrow_table[col])
            num_distinct = len(distinct)
            if num_distinct <= 10:
                sample = distinct
            else:
                sample = list(arrow_table[col].slice(0, 10))

            type_guesses, bad_data_count = guess_col_type(list(distinct))
            col_data = {
                'distinct': num_distinct,
                'sample': sample,
                'type': type_guesses,
                'skipped': bad_data_count
            }
            metadata['columns'][col] = col_data
    except Exception:
        logger.debug(metadata)
        logger.exception('Error getting metadata stats')
        raise
    return metadata
